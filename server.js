const express = require('express');

const bodyParser = require('body-parser');
const cors = require('cors');
const routes = require('./routes/index');
const helmet = require('helmet')
const cookieParser = require('cookie-parser')
const fileupload = require("express-fileupload");
const app = express()

var server;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(cookieParser())
app.use(helmet())
app.use(fileupload());
const APP_PORT = process.env.PORT || 3000;
// const savelog = require('./middleware/savelog')

// app.use(savelog)
routes(app);

async function startup() {
    console.log('Starting application');
    try {
  

      server = app.listen(APP_PORT, () =>
        console.log('SNN  Service Listening on port ' + APP_PORT)
      );
    } catch (err) {
      console.error(err);
      process.exit(1);
    }
  }


  async function shutdown(signal) {
    console.log(`Received ${signal} at ${new Date().toISOString()}`);
    try {
     
      console.log('connection pool closed');
      await server.close();
      console.log('Web server closed');
      process.exit(0);
    } catch (e) {
      console.error(e);
      process.exit(1);
    }
  }


process.on('SIGINT', shutdown);
process.on('SIGTERM', shutdown);
process.on('message', function(msg) {
  if (msg == 'shutdown') {
    setTimeout(function() {
      shutdown
    }, 1500);
  }
});
startup()
